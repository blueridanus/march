use crate::{
    camera::Camera,
    ray_marcher::{Ray, RayMarcher},
    scene::Scene,
    Point3, Point4,
};
use image::{ImageBuffer, Rgba};
use rayon::prelude::*;
use rayon::slice::ParallelSliceMut;

pub struct Renderer<'a> {
    ray_marcher: &'a RayMarcher,
    scene: &'a Scene,
    camera: &'a Camera,
    settings: RenderSettings,
}

pub struct RenderSettings {}

impl<'a> Renderer<'a> {
    pub fn new(ray_marcher: &'a RayMarcher, scene: &'a Scene, camera: &'a Camera, settings: RenderSettings) -> Self {
        Renderer {
            ray_marcher,
            scene,
            camera,
            settings,
        }
    }

    pub fn render_to_image(&self) -> ImageBuffer<Rgba<u8>, Vec<u8>> {
        let mut pixel_buffer = vec![0u8; self.camera.resolution.x as usize * self.camera.resolution.y as usize * 4];
        let projection = (self.camera.perspective.as_matrix() * self.camera.isometry.to_homogeneous()).try_inverse().unwrap();
        pixel_buffer.par_chunks_mut(4).enumerate().for_each(|(i, color)| {
            let x = i as u32 % self.camera.resolution.x;
            let y = i as u32 / self.camera.resolution.x;
            let i = ((x as f64 / self.camera.resolution.x as f64) - 0.5) * 2.0;
            let j = ((y as f64 / self.camera.resolution.y as f64) - 0.5) * 2.0;
            let p = Point4::new(i, j, -1., 1.);
            let target = Point3::from_homogeneous((projection * p).coords).unwrap();
            let eye = self.camera.isometry * Point3::origin();
            let mut alpha = 0;

            let ray = Ray::new(eye, (target - eye).normalize());
            let march_result = self.ray_marcher.march(ray.clone(), self.scene);

            if let Some(intersection) = march_result {
                let to_color = intersection.object.color(
                    intersection.at_point, 
                    ray.clone(), 
                    intersection.normal
                );
                
                color[0] = to_color[0];
                color[1] = to_color[1];
                color[2] = to_color[2];
                color[3] = to_color[3];
            }
        });
        ImageBuffer::from_raw(self.camera.resolution.x, self.camera.resolution.y, pixel_buffer).unwrap()
    }
}
