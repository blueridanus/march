use crate::render::Render;
use crate::{Point3, Vec3};
use crate::ray_marcher::Ray;

pub struct SdfCube {
    pub center: Point3,
    pub size: f64,
}

impl SdfCube {
    pub fn new(center: Point3, size: f64) -> Self {
        SdfCube {
            center,
            size,
        }
    }
}

impl Render for SdfCube {
    fn distance(&self, point: Point3) -> f64 {
        (nalgebra::Rotation3::new(Vec3::new(-0.2, 0.5, 0.3)) * (point-self.center)).amax() - self.size

    }

    fn color(&self, point: Point3, ray: Ray, normal: Vec3) -> [u8; 4] {
        let rot = nalgebra::Rotation3::new(Vec3::new(-0.3, 0.3, 0.0));
        let dot = nalgebra::dot(&normal, &(-(rot * ray.direction)));
        let brightness = (dot.max(0.0) * 255.0) as u8;
        [brightness, brightness, brightness, 255]
    }
}
