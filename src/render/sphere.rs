use crate::render::Render;
use crate::{Point3, Vec3};
use crate::ray_marcher::Ray;

pub struct Sphere {
    pub center: Point3,
    pub radius: f64,
}

impl Sphere {
    pub fn new(center: Point3, radius: f64) -> Self {
        Sphere {
            center,
            radius,
        }
    }
}

impl Render for Sphere {
    fn distance(&self, point: Point3) -> f64 {
        nalgebra::distance(&self.center, &point) - self.radius
    }

    fn color(&self, point: Point3, ray: Ray, normal: Vec3) -> [u8; 4] {
        let rot = nalgebra::Rotation3::new(Vec3::new(-0.3, 0.3, 0.0));
        let dot = nalgebra::dot(&normal, &(-(rot * ray.direction)));
        let brightness = (dot.max(0.0) * 255.0) as u8;
        [brightness, brightness, brightness, 255]
    }
}
