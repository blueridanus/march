use crate::{Point3, Vec3};
use crate::ray_marcher::Ray;

pub mod sphere;
pub mod sdf_cube;

pub trait Render {
    fn distance(&self, point: Point3) -> f64;
    fn color(&self, point: Point3, ray: Ray, normal: Vec3) -> [u8; 4]; //TODO: change to material
}
