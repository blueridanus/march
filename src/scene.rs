use crate::render::Render;

pub struct Scene {
    pub objects: Vec<Box<dyn Render + Sync>>,
}

impl Scene {
    pub fn new() -> Self {
        Scene {
            objects: vec![],
        }
    }

    pub fn add_object(&mut self, object: impl Render + Sync + 'static) {
        self.objects.push(Box::new(object));
    }
}
