mod camera;
mod ray_marcher;
mod render;
mod renderer;
mod scene;

use crate::{
    camera::{Camera, Resolution},
    ray_marcher::RayMarcher,
    render::sphere::Sphere,
    renderer::RenderSettings,
    renderer::Renderer,
    scene::Scene,
};
use image::Rgba;
use nalgebra::Vector3;

pub type Vec3 = Vector3<f64>;
pub type Point3 = nalgebra::Point3<f64>;
pub type Point4 = nalgebra::Point4<f64>;
pub type Isometry3 = nalgebra::Isometry3<f64>;
pub type Perspective3 = nalgebra::Perspective3<f64>;

fn main() {
    let ray_marcher = RayMarcher::new(200, 1e-4);

    let mut scene = Scene::new();
    scene.add_object(Sphere::new(Point3::origin(), 1.));
    //scene.add_object(Sphere::new(Point3::new(-10., 0., 0.), 10.));
    //scene.add_object(Sphere::new(Point3::new(100., -10., 10.), 100.));

    let camera = Camera::new(Isometry3::new(Vector3::new(0., 0., -10.), Vector3::zeros()), 45., 5., 100000., Resolution::new(100, 100));

    let renderer = Renderer::new(&ray_marcher, &scene, &camera, RenderSettings {});

    let image = renderer.render_to_image();
    image.save("test.png").unwrap();
}
