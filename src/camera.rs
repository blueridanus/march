use crate::{Isometry3, Perspective3};

pub struct Camera {
    pub isometry: Isometry3,
    pub perspective: Perspective3,
    pub resolution: Resolution,
}

pub struct Resolution {
    pub x: u32,
    pub y: u32,
}

impl Resolution {
    pub fn new(x: u32, y: u32) -> Self {
        Resolution {
            x,
            y,
        }
    }
}

impl Camera {
    pub fn new(isometry: Isometry3, fovy: f64, znear: f64, zfar: f64, resolution: Resolution) -> Self {
        Camera {
            isometry,
            perspective: Perspective3::new(resolution.x as f64 / resolution.y as f64, fovy.to_radians(), znear, zfar),
            resolution,
        }
    }
}
